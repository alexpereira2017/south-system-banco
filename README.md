# README #

### Visão Geral ###
Sistema bancário com cadastro de pessoa onde uma conta é criada automaticamente para cada novo usuário.
Após a criação da conta é realizado um cálculo para definir os limites de crédito.

O sistema foi dividido em dois repositórios:

Nome | Link Repositório
------------ | -------------
Banco | https://bitbucket.org/alexpereira2017/south-system-banco/src/master/
Limites | https://bitbucket.org/alexpereira2017/south-system-limites/src/master/

O Repositório **Banco** possui os endpoints para a criação de nova pessoa e listagem de contas e pessoas.

Endereço | Descrição
------------ | -------------
http://localhost:8080/cadastro/pessoa/ | Cria uma nova pessoa
http://localhost:8080/consultar/pessoas | Lista as pessoas cadastradas 
http://localhost:8080/consultar/contas | Lista as contas cadastradas
*Mais informações consultar a documentação no swagger (http://localhost:8080/swagger-ui.htm)

Foi criado um sistema de filas usando RabbitMQ. O sistema de **Limites** recebe dados de conta como o número da conta e score do cliente. Após, processa o cálculo de limites disponíveis para então retornar outra mensagem com o resultado obtido.  

Então, o sistema **Banco** realiza a leitura e persistes os dados no sistema.

### Como executar o programa ###
* Para ativar a aplicação Banco, usar o terminal: 
    1. $ git clone https://alexpereira2017@bitbucket.org/alexpereira2017/south-system-banco.git
    2. $ mvn spring-boot:run (obs: caso não tenha maven instalado pode usar o comando ./mvnw spring-boot:run
 no windows ou ./mvn spring-boot:run no linux)
     
* Para ativar o RabbitQM:
    1. Acessar o diretório do projeto onde se encontra o arquivo "docker-compose.yml" e no terminal executar:
        * $ docker-compose -up -d

* Para ativar a aplicação Limites, usar o terminal: 
    1. $ git clone https://alexpereira2017@bitbucket.org/alexpereira2017/south-system-limites.git
    2. $ mvn spring-boot:run  

### Outras questões ###

* O sistema está preparado para usar o banco de dados em memória "H2".
* É possível que na primeira execução ocorra erro por motido das filas ainda não terem sido criadas. A solução é encerrar e executar o programa novamente.
* Para fornecer uma massa de dados, ao subir o sistema já são cadastrados 12 pessoas e seus cartões. 
* Acesso à documentação http://localhost:8080/swagger-ui.htm
* Acesso ao gerenciador do RabbitQM: http://localhost:15672/
   * Usuário: guest
   * Senha: guest
