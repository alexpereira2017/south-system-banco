package com.southsystem.banco.service;

import com.southsystem.banco.domain.Conta;
import com.southsystem.banco.domain.Pessoa;
import com.southsystem.banco.domain.dto.LimiteClienteMessage;
import com.southsystem.banco.domain.enumeration.TipoConta;
import com.southsystem.banco.domain.enumeration.TipoPessoa;
import com.southsystem.banco.repository.ContaRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Value;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ContaServiceTest {

    @Mock
    public ContaRepository contaRepository;

    @InjectMocks
    public ContaService contaService;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void deve_inserir_uma_conta_com_sucesso() {

        Pessoa pessoa = Pessoa.builder().id(1).nome("Torre Hotel").escore(5).documento("55887799441")
                .tipo(TipoPessoa.FISICA).build();

        Conta conta = Conta.builder().id(1).agencia(1111).numero(22222222)
                .tipo(TipoConta.CORRENTE).limite_cartao(110).limite_cheque(500).build();
        when(contaRepository.save(any(Conta.class))).thenReturn(conta);

        Conta contaCriada = contaService.inserirNovaConta(pessoa);

        assertThat(contaCriada).isNotNull();
        verify(contaRepository).save(any(Conta.class));
    }

    @Test
    public void deve_montar_conta_com_sucesso() {
        Pessoa pessoa = Pessoa.builder().id(1).nome("Torre Hotel").escore(5).documento("55887799441")
                .tipo(TipoPessoa.FISICA).build();
        contaService.setNumeroAgencia(1245);

        final Conta conta = contaService.montarConta(pessoa);

        Assert.assertThat(conta.getAgencia(), is(1245));
        Assert.assertThat(conta.getNumero(), isA(Integer.class));
        Assert.assertThat(conta.getTipo(), is(TipoConta.CORRENTE));
    }

    @Test
    public void deve_retornar_cliente_empresarial_para_pessoa_juridica() {
        Pessoa pessoa = Pessoa.builder().tipo(TipoPessoa.JURIDICA).build();

        final TipoConta tipoConta = contaService.definirTipoConta(pessoa);

        Assert.assertThat(tipoConta, is(TipoConta.EMPRESARIAL));
    }

    @Test
    public void deve_retornar_conta_corrente_para_pessoa_fisica() {
        Pessoa pessoa = Pessoa.builder().tipo(TipoPessoa.FISICA).build();

        final TipoConta tipoConta = contaService.definirTipoConta(pessoa);

        Assert.assertThat(tipoConta, is(TipoConta.CORRENTE));
    }

    @Test
    public void deve_atualizar_limites() throws Exception {
        Conta conta = Conta.builder().id(1).numero(22222222).agencia(2222)
                .limite_cheque(200).limite_cartao(300).build();
        when(contaRepository.findById(anyInt())).thenReturn(Optional.of(conta));
        final LimiteClienteMessage message = LimiteClienteMessage
                .builder().conta_id(1).limiteCartao(800).limiteChequeEspecial(150).build();

        contaService.atualizarLimites(message);

        final ArgumentCaptor<Conta> contaArgumentCaptor = ArgumentCaptor.forClass(Conta.class);
        verify(contaRepository).save(contaArgumentCaptor.capture());
        final Conta contaAtualizada = contaArgumentCaptor.getValue();
        Assert.assertThat(contaAtualizada.getLimite_cartao(), is(800D));
        Assert.assertThat(contaAtualizada.getLimite_cheque(), is(150D));
    }

    @Test
    public void deve_gerar_excecao_por_nao_reconhecer_conta_ao_atualizar_limites() {
        when(contaRepository.findById(anyInt())).thenReturn(Optional.empty());
        final LimiteClienteMessage message = LimiteClienteMessage
                .builder().conta_id(1).limiteCartao(150).limiteChequeEspecial(150).build();

        try {
            contaService.atualizarLimites(message);
            fail();
        } catch (Exception e) {
            Assert.assertThat(e.getMessage(), startsWith("Não existe conta com o código"));
        }
    }
}
