package com.southsystem.banco.service;

import com.southsystem.banco.domain.Pessoa;
import com.southsystem.banco.domain.dto.PessoaDto;
import com.southsystem.banco.domain.enumeration.TipoPessoa;
import com.southsystem.banco.repository.PessoaRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PessoaServiceTest {

    @Mock
    private PessoaRepository pessoaRepository;

    @InjectMocks
    public PessoaService pessoaService;

    @Test
    public void deve_inserir_uma_pessoa_com_sucesso() {
        Pessoa pessoa = Pessoa.builder().id(1).nome("Torre Hotal").escore(5).documento("55887799441")
                .tipo(TipoPessoa.FISICA).build();
        PessoaDto pessoaDto = criarPessoaDto();

        when(pessoaRepository.save(any(Pessoa.class))).thenReturn(pessoa);

        final Pessoa pessoaCriada = pessoaService.inserirNovoCliente(pessoaDto);
        assertThat(pessoaCriada).isNotNull();
        verify(pessoaRepository).save(any(Pessoa.class));
    }

    @Test
    public void deve_converter_dto_com_sucesso() {
        PessoaDto pessoaDto = criarPessoaDto();

        final Pessoa pessoa = pessoaService.fromDTO(pessoaDto);

        Assert.assertThat(pessoa.getNome(), is(pessoaDto.getNome()));
        Assert.assertThat(pessoa.getEscore(), is(pessoaDto.getEscore()));
        Assert.assertThat(pessoa.getDocumento(), is(pessoaDto.getDocumento()));
        Assert.assertThat(pessoa.getTipo(), is(pessoaDto.getTipo()));
    }

    private PessoaDto criarPessoaDto() {
        return PessoaDto.builder().id(1).nome("Torre Hotal").escore(5).documento("55887799441")
                .tipo(TipoPessoa.FISICA).build();
    }


}
