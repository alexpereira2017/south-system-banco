package com.southsystem.banco.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.southsystem.banco.domain.enumeration.TipoPessoa;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Pessoa implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    private String nome;
    private TipoPessoa tipo;
    private String documento;
    private Integer escore;

    @OneToMany(mappedBy = "dono")
    @JsonIgnore
    private List<Conta> contas;
}
