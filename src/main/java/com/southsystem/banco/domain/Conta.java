package com.southsystem.banco.domain;

import com.southsystem.banco.domain.enumeration.TipoConta;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Conta implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;
    private Integer agencia;
    private Integer numero;
    private TipoConta tipo;
    private double limite_cartao;
    private double limite_cheque;

    @ManyToOne
    @JoinColumn(name="pessoa_id")
    private Pessoa dono;
}
