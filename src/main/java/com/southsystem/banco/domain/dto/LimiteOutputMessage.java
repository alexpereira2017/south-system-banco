package com.southsystem.banco.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;


@ToString
@Getter
public class LimiteOutputMessage {
    private final int id;
    private final int escore;

    public LimiteOutputMessage(@JsonProperty("id") int id,
                               @JsonProperty("escore") int escore) {
        this.id = id;
        this.escore = escore;
    }
}
