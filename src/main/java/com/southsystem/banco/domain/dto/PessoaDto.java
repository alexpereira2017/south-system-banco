package com.southsystem.banco.domain.dto;

import com.southsystem.banco.domain.enumeration.TipoPessoa;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PessoaDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;

    @NotEmpty(message="Informe o nome da pessoa")
    private String nome;

    @NotNull
    private TipoPessoa tipo;

    @NotEmpty(message="Informe o número do documento")
    private String documento;

    @Min(0)
    @Max(9)
    private Integer escore;
}
