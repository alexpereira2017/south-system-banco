package com.southsystem.banco.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@Builder
public class LimiteClienteMessage {
    private int conta_id;
    private int escore;
    private double limiteCartao;
    private double limiteChequeEspecial;

    public LimiteClienteMessage(@JsonProperty("id") int conta_id,
                                @JsonProperty("escore") int escore,
                                @JsonProperty("limiteCartao") double limiteCartao,
                                @JsonProperty("limiteChequeEspecial") double limiteChequeEspecial) {
        this.conta_id = conta_id;
        this.escore = escore;
        this.limiteCartao = limiteCartao;
        this.limiteChequeEspecial = limiteChequeEspecial;
    }

}
