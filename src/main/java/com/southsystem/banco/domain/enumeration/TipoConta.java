package com.southsystem.banco.domain.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
@Getter
public enum TipoConta {
    CORRENTE(0, "Conta Corrente"),
    EMPRESARIAL(1, "Conta Empresarial");

    private int cod;
    private String descricao;
}


