package com.southsystem.banco.domain.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoPessoa {
    FISICA(0, "Pessoa Física"),
    JURIDICA(1, "Pessoa Jurídica");

    private int cod;
    private String descricao;
}
