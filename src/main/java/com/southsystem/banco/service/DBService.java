package com.southsystem.banco.service;

import com.southsystem.banco.domain.Pessoa;
import com.southsystem.banco.domain.dto.LimiteOutputMessage;
import com.southsystem.banco.domain.dto.PessoaDto;
import com.southsystem.banco.domain.enumeration.TipoPessoa;
import com.southsystem.banco.producer.RabbitMQProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DBService {

    @Autowired
    private PessoaService pessoaService;

    @Autowired
    private ContaService contaService;

    @Autowired
    private RabbitMQProducer messageProducer;

    public void instantiateData() {
        List<PessoaDto> pessoas = new ArrayList<>();
        pessoas.add(new PessoaDto(1, "Lorival", TipoPessoa.FISICA, "55214789658", 3));
        pessoas.add(new PessoaDto(2,"Magazine Luiza", TipoPessoa.JURIDICA, "12524787000123", 4));
        pessoas.add(new PessoaDto(3,"Petrobras", TipoPessoa.JURIDICA, "23213321000123", 5));
        pessoas.add(new PessoaDto(4,"Lojas Renner", TipoPessoa.JURIDICA, "88987654000123", 6));
        pessoas.add(new PessoaDto(5,"Maria Fernandes", TipoPessoa.FISICA, "52147845773", 7));
        pessoas.add(new PessoaDto(6,"Antonio Pessoa", TipoPessoa.FISICA, "02156754870", 8));
        pessoas.add(new PessoaDto(7,"Alfredo Fagundes", TipoPessoa.FISICA, "12987001547", 9));
        pessoas.add(new PessoaDto(8,"Gol Aviação", TipoPessoa.JURIDICA, "55987456000123", 0));
        pessoas.add(new PessoaDto(9,"San Martin SA", TipoPessoa.JURIDICA, "55552234000123", 1));
        pessoas.add(new PessoaDto(10,"Leopoldo Pereira", TipoPessoa.FISICA, "52141287459", 2));
        pessoas.add(new PessoaDto(11,"Marta Leonora", TipoPessoa.FISICA, "25147854713", 3));
        pessoas.add(new PessoaDto(12,"Via Varejo", TipoPessoa.JURIDICA, "12524787000123", 4));
        pessoas.forEach(dto -> {
            pessoaService.inserirNovoCliente(dto);
            Pessoa p = pessoaService.fromDTO(dto);
            contaService.inserirNovaConta(p);
            messageProducer.produce(new LimiteOutputMessage(p.getId(), p.getEscore()));
        });

    }
}
