package com.southsystem.banco.service;

import com.southsystem.banco.domain.Conta;
import com.southsystem.banco.domain.Pessoa;
import com.southsystem.banco.domain.dto.LimiteClienteMessage;
import com.southsystem.banco.domain.enumeration.TipoConta;
import com.southsystem.banco.domain.enumeration.TipoPessoa;
import com.southsystem.banco.repository.ContaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ContaService {

    private final ContaRepository contaRepository;

    @Value("${agencia}")
    private Integer numeroAgencia;

    public void setNumeroAgencia(Integer numeroAgencia) {
        this.numeroAgencia = numeroAgencia;
    }

    public Conta inserirNovaConta(Pessoa pessoa) {
        Conta conta = montarConta(pessoa);
        conta.setDono(pessoa);
        return contaRepository.save(conta);
    }

    public Conta montarConta(Pessoa pessoa) {
        final Conta conta = new Conta();
        conta.setAgencia(numeroAgencia);
        conta.setNumero(novoNumeroConta());
        conta.setTipo(definirTipoConta(pessoa));
        return conta;
    }

    private int novoNumeroConta() {
        return contaRepository.countContaByAgencia(numeroAgencia) + 1;
    }

    public TipoConta definirTipoConta(Pessoa pessoa) {
        if (pessoa.getTipo().getCod() == TipoPessoa.JURIDICA.getCod()) {
            return TipoConta.EMPRESARIAL;
        }
        return TipoConta.CORRENTE;
    }

    public void atualizarLimites(LimiteClienteMessage in) throws Exception {
        final Optional<Conta> byId = contaRepository.findById(in.getConta_id());
        if (byId.isPresent()) {
            Conta conta = byId.get();
            conta.setLimite_cartao(in.getLimiteCartao());
            conta.setLimite_cheque(in.getLimiteChequeEspecial());
            contaRepository.save(conta);
        } else {
            throw new Exception(
                    String.format("Não existe conta com o código (%s) na base de dados",
                            in.getConta_id()));
        }
    }

    public List<Conta> pesquisar() {
        return contaRepository.findAll();
    }
}
