package com.southsystem.banco.service;

import com.southsystem.banco.domain.Pessoa;
import com.southsystem.banco.domain.dto.PessoaDto;
import com.southsystem.banco.repository.PessoaRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PessoaService {

    private final PessoaRepository pessoaRepository;

    public Pessoa inserirNovoCliente(PessoaDto pessoaDto) {
        Pessoa pessoa = fromDTO(pessoaDto);
        pessoaRepository.save(pessoa);
        return pessoa;
    }

    public Pessoa fromDTO(PessoaDto dto) {
        ModelMapper mapper = new ModelMapper();
        Pessoa obj = mapper.map(dto, Pessoa.class);
        return obj;
    }

    public List<Pessoa> pesquisar() {
        return pessoaRepository.findAll();
    }
}
