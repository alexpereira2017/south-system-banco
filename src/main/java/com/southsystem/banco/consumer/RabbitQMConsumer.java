package com.southsystem.banco.consumer;

import com.southsystem.banco.domain.dto.LimiteClienteMessage;
import com.southsystem.banco.service.ContaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RabbitQMConsumer {

    @Autowired
    private ContaService contaService;

    @RabbitListener(queues = "${rabbitmq.queue.in}")
    public void listen(final LimiteClienteMessage in) {
        log.info("Message read from myQueue : " + in);
        try {
            contaService.atualizarLimites(in);
        } catch (Exception e) {
            log.warn(e.getMessage());
        }
    }
}
