package com.southsystem.banco.resource;

import com.southsystem.banco.domain.Pessoa;
import com.southsystem.banco.domain.dto.LimiteOutputMessage;
import com.southsystem.banco.domain.dto.PessoaDto;
import com.southsystem.banco.producer.RabbitMQProducer;
import com.southsystem.banco.service.ContaService;
import com.southsystem.banco.service.PessoaService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping(value="/cadastro")
public class CadastrosResource {

    @Autowired
    private PessoaService pessoaService;

    @Autowired
    private ContaService contaService;

    @Autowired
    private RabbitMQProducer messageProducer;

    @ApiOperation(value="Endpoint em REST que insere uma nova pessoa e vincula para ela uma conta.")
    @RequestMapping(value = "/pessoa/", method = RequestMethod.POST)
    public ResponseEntity<Void> novaPessoa(@Valid @RequestBody PessoaDto pessoaDto) {
        Pessoa obj = pessoaService.inserirNovoCliente(pessoaDto);
        contaService.inserirNovaConta(obj);
        messageProducer.produce(new LimiteOutputMessage(obj.getId(), obj.getEscore()));
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(obj.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

}
