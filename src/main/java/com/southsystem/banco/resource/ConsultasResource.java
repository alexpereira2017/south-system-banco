package com.southsystem.banco.resource;

import com.southsystem.banco.domain.Conta;
import com.southsystem.banco.domain.Pessoa;
import com.southsystem.banco.service.ContaService;
import com.southsystem.banco.service.PessoaService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value="/consultar")
public class ConsultasResource {

    @Autowired
    private PessoaService pessoaService;

    @Autowired
    private ContaService contaService;

    @ApiOperation(value="Endpoint em REST que liste as pessoas")
    @RequestMapping(value = "/pessoas", method= RequestMethod.GET)
    public ResponseEntity<List<Pessoa>> findAll() {
        List<Pessoa> list = pessoaService.pesquisar();
        return ResponseEntity.ok().body(list);
    }

    @ApiOperation(value="Endpoint em REST que lista as contas e seus respectivos limites / cartões")
    @RequestMapping(value = "/contas", method= RequestMethod.GET)
    public ResponseEntity<List<Conta>> findAllAccounts() {
        List<Conta> list = contaService.pesquisar();
        return ResponseEntity.ok().body(list);
    }
}
