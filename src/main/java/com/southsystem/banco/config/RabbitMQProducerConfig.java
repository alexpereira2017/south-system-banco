package com.southsystem.banco.config;



import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class RabbitMQProducerConfig {

    @Value("${rabbitmq.queue.in}")
    String queueNameInput;

    @Value("${rabbitmq.queue.out}")
    String queueNameOutput;

    @Value("${rabbitmq.exchange}")
    String exchange;

    @Value("${rabbitmq.routingkey}")
    private String routingkey;

    @Bean
    public Queue queueInput() {
        return new Queue(queueNameInput, false);
    }

    @Bean
    public Queue queueOutput() {
        return new Queue(queueNameOutput, false);
    }

    @Bean("exchangeCreate")
    public DirectExchange exchange() {
        return new DirectExchange(exchange);
    }

    @Bean
    public Binding bindingInput(@Qualifier("queueInput") Queue queue, DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(routingkey);
    }

    @Bean
    public Binding bindingOutput(@Qualifier("queueOutput") Queue queue, DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(routingkey);
    }
}
